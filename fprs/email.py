import logging
from django.utils.timezone import now
from django.contrib.sites.models import Site

from backend.shortcuts import build_absolute_uri
from nm2.lib.email import build_django_message

log = logging.getLogger(__name__)


def notify_new_endorsement(endorsement, request=None, notify_ml="newmaint", date=None):
    """
    Render a notification email template for a newly uploaded endorsement, then
    send the resulting email.
    """
    if date is None:
        date = now()

    if request is None:
        url = "https://{}{}".format(
            Site.objects.get_current().domain,
            endorsement.get_absolute_url())
    else:
        url = build_absolute_uri(endorsement.get_absolute_url(), request)

    import django.utils.translation as translation
    with translation.override('en'):
        body = """{endorsement.text}

{endorsement.author.fullname} (via nm.debian.org)

For details and to comment, visit {url}
"""
        body += "-- \n"
        body += "{url}\n"
        body = body.format(endorsement=endorsement, url=url)

        to = "debian-{}@lists.debian.org".format(notify_ml)
        cc = [endorsement.fingerprint.person, endorsement.author]

        headers = {}
        msg = build_django_message(
            from_email=(f"{endorsement.author.fullname} (via nm.debian.org)", "nm@debian.org"),
            to=to,
            cc=cc,
            reply_to=[to]+cc,
            subject="Endorsing {}'s key {}".format(
                endorsement.fingerprint.person.fullname,
                endorsement.fingerprint.fpr,
                ),
            date=date,
            headers=headers,
            body=body)
    msg.send()
    log.debug("sent mail from %s to %s cc %s bcc %s reply-to: %s subject %s",
              msg.from_email,
              ", ".join(msg.to),
              ", ".join(msg.cc),
              ", ".join(msg.bcc),
              ", ".join(msg.reply_to),
              msg.subject)
