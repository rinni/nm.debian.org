from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^keycheck/(?P<fpr>[0-9A-Fa-f]{32,40})$', views.Keycheck.as_view(), name="keyring_keycheck"),
]
