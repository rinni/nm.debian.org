from __future__ import annotations
from django.test import TestCase
from deploy.deployer import Deployer, Fail
from contextlib import contextmanager
import tempfile
import git
import os


class Env:
    def __init__(self, workdir):
        self.workdir = workdir
        self.queuedir = os.path.join(self.workdir, "queue")
        os.mkdir(self.queuedir)
        self.repodir = os.path.join(self.workdir, "repo")
        self.main_hexsha = None

    def create_repo(self):
        repo = git.Repo.init(self.repodir, bare=False, initial_branch='main')
        commit = repo.index.commit("Creating a branch")
        self.main_hexsha = commit.hexsha
        # Not available on current debian stable
        # repo.close()


class TestDeploy(TestCase):
    @contextmanager
    def env(self):
        with tempfile.TemporaryDirectory() as workdir:
            env = Env(workdir)
            env.create_repo()
            with self.settings(DEPLOY_QUEUE_DIR=env.queuedir):
                yield env

    def test_select_branch(self):
        with self.env() as env:
            with open(os.path.join(env.queuedir, env.main_hexsha + ".deploy"), "w"):
                pass
            with open(os.path.join(env.queuedir, "1111111111111111111111111111111111111111.deploy"), "w"):
                pass
            with self.settings(DEPLOY_BRANCH="main"):
                deployer = Deployer(root=env.repodir)
                deployer.select_branch(fetch=False)
                self.assertEqual(deployer.branch, "main")
                self.assertEqual(deployer.ref.commit.hexsha, env.main_hexsha)

    def test_select_branch_fail(self):
        with self.env() as env:
            with open(os.path.join(env.queuedir, "1111111111111111111111111111111111111111.deploy"), "w"):
                pass
            with self.settings(DEPLOY_BRANCH="main"):
                deployer = Deployer(root=env.repodir)
                with self.assertRaises(Fail) as e:
                    deployer.select_branch(fetch=False)
                self.assertIn("are queued for deploy", str(e.exception))
