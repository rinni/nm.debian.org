from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^gitlab-pipeline-hook$', views.GitlabPipeline.as_view(), name="deploy_gitlab_pipeline_hook"),
]
