from __future__ import annotations
from django import http
from django.core.exceptions import PermissionDenied
from django.forms.models import model_to_dict
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
import backend.models as bmodels
from apikeys.mixins import APIVisitorMixin
import json
import logging

log = logging.getLogger(__name__)


class Serializer(json.JSONEncoder):
    def default(self, o):
        if hasattr(o, "strftime"):
            return o.strftime("%s")
            # return o.strftime("%Y-%m-%d %H:%M:%S")
        return json.JSONEncoder.default(self, o)


def json_response(val, status_code=200):
    res = http.HttpResponse(content_type="application/json")
    res.status_code = status_code
    json.dump(val, res, cls=Serializer, indent=1)
    return res


def person_to_json(p: bmodels.Person, **kw):
    res = model_to_dict(p, **kw)
    res["fullname"] = p.fullname
    res["url"] = p.get_absolute_url()
    res["fpr"] = p.fpr
    res["uid"] = p.ldap_fields.uid
    res["cn"] = p.ldap_fields.cn
    res["mn"] = p.ldap_fields.mn
    res["sn"] = p.ldap_fields.sn
    return res


class People(APIVisitorMixin, APIView):
    def get(self, request, *args, **kw):
        # Pick what to include in the result based on auth status
        fields = ["fpr", "status", "status_changed", "created", "status_description"]
        if self.request.user.is_authenticated and self.request.user.is_dd:
            fields.append("email")
            if self.request.user.is_superuser:
                fields.append("fd_comment")

        try:
            res = []

            # Build query
            people = bmodels.Person.objects.all()

            val = request.GET.get("email", None)
            if val is not None:
                if val.startswith("/") and val.endswith("/"):
                    people = people.filter(email__iregex=val[1:-1])
                else:
                    people = people.filter(email__iexact=val)

            val = request.GET.get("uid", None)
            if val is not None:
                if val.startswith("/") and val.endswith("/"):
                    people = people.filter(ldap_fields__uid__iregex=val[1:-1])
                else:
                    people = people.filter(ldap_fields__uid__iexact=val)

            val = request.GET.get("name", None)
            if val is not None:
                if val.startswith("/") and val.endswith("/"):
                    val = val[1:-1]
                    people = people.filter(fullname__iregex=val)
                else:
                    people = people.filter(fullname__iexact=val)

            val = request.GET.get("fpr", None)
            if val is not None:
                people = people.filter(fprs__fpr__endswith=val)

            val = request.GET.get("status", None)
            if val is not None:
                people = people.filter(status=val)

            if self.request.user.is_superuser:
                val = request.GET.get("fd_comment", "")
                if val:
                    people = people.filter(fd_comment__icontains=val)

            for p in people.select_related("ldap_fields").order_by("fullname"):
                res.append(person_to_json(p, fields=fields))

            return json_response(dict(r=res))
        except Exception as e:
            # import traceback
            # traceback.print_exc()
            return json_response(dict(e=str(e)), status_code=500)


class Status(APIVisitorMixin, APIView):
    def _serialize_people(self, people):
        res = {}
        for p in people:
            debsso_identity = p.identities.filter(issuer="debsso").first()
            if debsso_identity is None:
                continue
            perms = p.perms
            rp = {
                "status": p.status,
            }
            if "am" in perms:
                rp["is_am"] = True
            res[debsso_identity.subject] = rp
        return json_response(dict(people=res))

    def get(self, request, *args, **kw):
        q_status = request.GET.get("status", None)
        q_person = request.GET.get("person", None)
        q_all = not any(bool(x) for x in (q_status, q_person))

        # Ensure that we got only one query
        if sum(bool(x) for x in (q_status, q_person)) > 1:
            return http.HttpResponseBadRequest("only one of status, person can be specified")

        # Enforce access restrictions
        if (q_status or q_all) and not self.request.user.is_authenticated:
            raise PermissionDenied

        # Get a QuerySet with the people to return
        persons = bmodels.Person.objects.all()
        if q_status:
            persons = persons.filter(status__in=q_status.split(","))
        elif q_person:
            persons = persons.filter(identities__issuer="debsso", identities__subject__in=q_person.split(","))
        elif q_all:
            pass
        else:
            return http.HttpResponseServerError("request cannot be understood")

        return self._serialize_people(persons)

    def post(self, request, *args, **kw):
        names = [str(x) for x in json.loads(request.body.decode("utf8"))]
        persons = bmodels.Person.objects.filter(identities__issuer="debsso", identities__subject__in=names)
        return self._serialize_people(persons)

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(Status, self).dispatch(*args, **kwargs)


class Whoami(APIVisitorMixin, APIView):
    """
    Return a JSON with information on the currently logged in user
    """
    def get(self, request, *args, **kw):
        if request.user.is_authenticated:
            data = model_to_dict(
                    self.request.user, fields=["cn", "mn", "sn", "email", "uid", "status", "status_changed"])
            data["fpr"] = self.request.user.fpr
            data["identities"] = [
                model_to_dict(i, fields=[
                    "issuer", "subject", "last_used", "profile", "picture", "fullname", "username"])
                for i in self.request.user.identities.all()
            ]
        else:
            data = {}
        res = http.HttpResponse(content_type="application/json")
        json.dump(data, res, indent=1, cls=Serializer)
        return res


class ExportIdentities(APIVisitorMixin, APIView):
    """
    Return a JSON with identity and status information for all users
    """
    def get(self, request, *args, **kw):
        from signon.models import Identity
        from backend import const

        if not self.request.user.is_authenticated or not self.request.user.is_staff:
            for addr in settings.IDENTITIES_EXPORT_ALLOW_IPS:
                log.debug("Trying to match %s against %s", addr, request.META["REMOTE_ADDR"])
                if request.META["REMOTE_ADDR"] == addr:
                    break
            else:
                log.warning("Remote address %s does not match %r",
                            request.META["REMOTE_ADDR"], settings.IDENTITIES_EXPORT_ALLOW_IPS)
                raise PermissionDenied

        by_person = {}
        for identity in Identity.objects.select_related("person", "person__ldap_fields").all():
            person = identity.person
            if person is None:
                continue

            person_data = by_person.get(person.pk)
            if person_data is None:
                if person.is_dd:
                    status = "debian_developer"
                elif person.status in (const.STATUS_DM, const.STATUS_DM_GA):
                    status = "debian_maintainer"
                elif person.status in (const.STATUS_EMERITUS_DD,):
                    status = "debian_emeritus"
                elif person.status in (const.STATUS_REMOVED_DD,):
                    status = "debian_removed_dd"
                else:
                    continue

                person_data = {
                    "full_name": person.fullname,
                    "email": person.email,
                    "status": status,
                    "ldap_uid": person.get_ldap_uid(),
                    "identities": [],
                }
                by_person[person.pk] = person_data

            person_data["identities"].append({
                "issuer": identity.issuer,
                "subject": identity.subject,
                "username": identity.username,
            })

        res = http.HttpResponse(content_type="application/json")
        json.dump(list(by_person.values()), res, indent=1, cls=Serializer)
        return res


class SalsaStatus(APIVisitorMixin, APIView):
    def get(self, request, *args, **kw):
        from signon.models import Identity
        identity = get_object_or_404(Identity, issuer="salsa", subject=self.kwargs["subject"])
        if identity.person is None:
            data = {
                "profile": None,
                "status": None,
                "uid": None,
            }
        else:
            data = {
                "profile": identity.person.get_absolute_url(),
                "status": identity.person.status,
                "uid": identity.person.get_ldap_uid(),
            }
        res = http.HttpResponse(content_type="application/json")
        json.dump(data, res, indent=1, cls=Serializer)
        return res

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)
