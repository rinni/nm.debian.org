from __future__ import annotations
from django import http
from django.core.exceptions import ImproperlyConfigured
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.views.generic import View
from django.contrib.sites.shortcuts import get_current_site
import backend.const as const
from backend.mixins import VisitorMixin
import signon.models as smodels
import json
import time
import logging

log = logging.getLogger(__name__)


class DBExport(VisitorMixin, View):
    require_visitor = "dd"

    def get(self, request, *args, **kw):
        from backend.export import export_db

        exported = export_db(full=False)

        class Serializer(json.JSONEncoder):
            def default(self, o):
                if hasattr(o, "strftime"):
                    return o.strftime("%Y-%m-%d %H:%M:%S")
                return json.JSONEncoder.default(self, o)

        res = http.HttpResponse(content_type="application/json")
        res["Content-Disposition"] = "attachment; filename=nm-mock.json"
        json.dump(exported, res, cls=Serializer, indent=1)
        return res


class SalsaExport(VisitorMixin, View):
    def get(self, request, *args, **kw):
        site = get_current_site(request)
        salsa_host = settings.SALSA_HOST

        if not self.request.user.is_authenticated or not self.request.user.is_staff:
            for addr in settings.SALSA_EXPORT_ALLOW_IPS:
                log.debug("Trying to match %s against %s", addr, request.META["REMOTE_ADDR"])
                if request.META["REMOTE_ADDR"] == addr:
                    break
            else:
                log.warning("Remote address %s does not match %r",
                            request.META["REMOTE_ADDR"], settings.SALSA_EXPORT_ALLOW_IPS)
                raise PermissionDenied

        signed = request.GET.get("signed") is not None
        if signed:
            import jwcrypto.jws
            import jwcrypto.jwk
            from jwcrypto.common import json_encode

            key_json = getattr(settings, "SIGNON_KEY", None)
            if key_json is None:
                raise ImproperlyConfigured("Using salsa export but SIGNON_KEY not set")
            key = jwcrypto.jwk.JWK.from_json(key_json)

        persons = []
        for identity in (
                smodels.Identity.objects.filter(issuer="salsa")
                                        .select_related("person", "person__ldap_fields")):
            person = identity.person
            if person is None:
                continue
            elif person.is_dd:
                status = "debian_developer"
            elif person.status in (const.STATUS_DM, const.STATUS_DM_GA):
                status = "debian_maintainer"
            elif person.status in (const.STATUS_EMERITUS_DD,):
                status = "debian_emeritus"
            elif person.status in (const.STATUS_REMOVED_DD,):
                status = "debian_removed_dd"
            else:
                continue

            person_data = {
                "aud": salsa_host,
                "sub": identity.subject,
                "https://nm.debian.org/claims/debian_status": status,
                "profile": request.build_absolute_uri(person.get_absolute_url()),
            }
            if person.is_dd:
                person_data["email"] = f"{person.ldap_fields.uid}@debian.org"
            persons.append(person_data)

        exported = {
            "iss": f"https://{site.domain}",
            "exp": int(time.time()) + 3600 * 24 * 3,
            "https://nm.debian.org/claims/persons": persons,
        }

        if signed:
            token = jwcrypto.jws.JWS(json_encode(exported))
            token.add_signature(
                    key, None,
                    {"alg": "RS256"},
                    {"kid": key.thumbprint()})

            return http.HttpResponse(token.serialize(), content_type="application/json")
        else:
            res = http.HttpResponse(content_type="application/json")
            json.dump(exported, res, indent=1)
            return res
