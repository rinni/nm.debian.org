from __future__ import annotations
from django.utils.translation import ugettext as _
from django.views.generic import TemplateView
from django.core.exceptions import PermissionDenied
from django.core import signing
from django.urls import reverse
from . import models as bmodels
from nmlayout.mixins import NM2LayoutMixin, NavLink


class OverrideView(Exception):
    """
    Allow to override the current view using a different view function in
    load_objects, check_permissions and pre_dispatch.

    This can be used to show nice error messages when special cases are
    detected.
    """
    def __init__(self, method):
        self.method = method


class VisitorMixin(NM2LayoutMixin):
    """
    Base permission-aware and context-aware view structure
    """
    # Define to "dd" "am" or "admin" to raise PermissionDenied if the
    # given test on the visitor fails
    require_visitor = None

    def load_objects(self):
        """
        Hook to set self.* members from request parameters, so that they are
        available to the rest of the view members.
        """
        pass

    def check_permissions(self, request=None):
        """
        Raise PermissionDenied if some of the permissions requested by the view
        configuration are not met.

        Subclasses can extend this to check their own permissions.
        """
        if request is not None:
            # Hack to play well as a mixin in rest_framework's ApiView
            super().check_permissions(request)

        if self.require_visitor and (
                (not self.request.user.is_authenticated)
                or self.require_visitor not in self.request.user.perms):
            raise PermissionDenied

    def pre_dispatch(self):
        pass

    def dispatch(self, request, *args, **kwargs):
        try:
            self.load_objects()
            self.check_permissions()
            self.pre_dispatch()
        except OverrideView as e:
            return e.method(request, *args, **kwargs)

        return super(VisitorMixin, self).dispatch(request, *args, **kwargs)


class VisitorTemplateView(VisitorMixin, TemplateView):
    pass


class VisitPersonMixin(VisitorMixin):
    """
    Visit a person record. Adds self.person and self.visit_perms with the
    permissions the visitor has over the person
    """
    # Define to "edit_bio" "edit_ldap" or "view_person_audit_log" to raise
    # PermissionDenied if the given test on the person-visitor fails
    require_visit_perms = None

    def get_person(self):
        """
        Return the person visited by this view
        """
        key = self.kwargs.get("key", None)
        if key is not None:
            return bmodels.Person.lookup_or_404(key)

        # Default to the current user
        if not self.request.user.is_authenticated:
            raise PermissionDenied
        return self.request.user

    def get_person_menu_entries(self):
        res = super().get_person_menu_entries()
        res.append(NavLink(self.person.get_ddpo_url(), _("DDPO"), "tasks"))
        res.append(NavLink(self.person.get_portfolio_url(), _("Portfolio"), "newspaper-o"))
        url = self.person.get_contributors_url()
        if url:
            disabled = False
        else:
            disabled = True
            url = "#"
        res.append(NavLink(url, _("Contributor"), "address-card", disabled))
        if self.request.user.is_authenticated:
            res.append(NavLink(
                reverse("minechangelogs:search", kwargs={"key": self.person.lookup_key}), _("Changelogs"), "history"))
            if self.request.user.is_superuser:
                res.append(NavLink(
                    self.person.get_admin_url(), _("Admin"), "microchip"))
                if "am_candidate" in self.person.perms:
                    res.append(NavLink(
                        reverse("admin:backend_am_add") + f"?person={self.person.id}", _("Make AM")))
                if self.person.is_dd:
                    res.append(NavLink(
                        reverse("mia:wat_ping", kwargs={"key": self.person.lookup_key}), _("WAT ping"), "heartbeat"))
                    from process.views import Emeritus
                    emeritus_link = Emeritus.get_nonauth_url(self.person, self.request)
                    if emeritus_link:
                        res.append(NavLink(emeritus_link, _("One click emeritus"), "bed"))
                if self.person.is_am:
                    res.append(NavLink(
                        reverse("person:amprofile", kwargs={"key": self.person.lookup_key}), _("AM Profile"), "gear"))
            if "view_person_audit_log" in self.visit_perms:
                res.append(NavLink(
                    reverse("person:identities", kwargs={
                        "key": self.person.lookup_key}), _("Logins"), "sign-in"))
        return res

    def get_visit_perms(self):
        return self.person.permissions_of(self.request.user)

    def load_objects(self):
        super().load_objects()
        self.person = self.get_person()
        self.visit_perms = self.get_visit_perms()

    def check_permissions(self):
        super().check_permissions()
        if self.require_visit_perms and self.require_visit_perms not in self.visit_perms:
            raise PermissionDenied

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["person"] = self.person
        ctx["visit_perms"] = self.visit_perms
        return ctx


class VisitPersonTemplateView(VisitPersonMixin, TemplateView):
    pass


class TokenAuthMixin:
    # Domain used for this token
    token_domain = None
    # Max age in seconds
    token_max_age = 15 * 3600 * 24

    @classmethod
    def make_token(cls, uid, **kw):
        from django.utils.http import urlencode
        kw.update(u=uid, d=cls.token_domain)
        return urlencode({"t": signing.dumps(kw)})

    def verify_token(self, decoded):
        # Extend to verify extra components of the token
        pass

    def load_objects(self):
        token = self.request.GET.get("t")
        if token is not None:
            try:
                decoded = signing.loads(token, max_age=self.token_max_age)
            except signing.BadSignature:
                raise PermissionDenied
            uid = decoded.get("u")
            if uid is None:
                raise PermissionDenied
            if decoded.get("d") != self.token_domain:
                raise PermissionDenied
            self.verify_token(decoded)
            try:
                u = bmodels.Person.objects.get(ldap_fields__uid=uid)
            except bmodels.Person.DoesNotExist:
                u = None
            if u is not None:
                self.request.user = u
        super().load_objects()
