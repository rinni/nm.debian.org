from django.test import TestCase
from backend.models import Person
from dsa.models import LDAPFields
import datetime

from nm2.lib.email import _to_django_addr, build_python_message


def parse_headers(bytemsg):
    headers = []
    body = []
    in_body = False
    for line in bytemsg.splitlines():
        if in_body:
            body.append(line)
            continue
        if not line:
            in_body = True
            continue
        if line[0] in (ord(b' '), ord(b'\t')):
            headers[-1] += line
        else:
            headers.append(line)
    return dict(x.split(b": ", 1) for x in headers), body


class TestEmail(TestCase):
    def setUp(self):
        self.person = Person.objects.create_user(fullname="Ondřej Nový", email="ondrej@example.org", audit_skip=True)
        LDAPFields.objects.create(
                person=self.person, cn="Ondřej", sn="Nový", email="ondrej@example.org", audit_skip=True)
        self.addrstr = "test@example.org"
        self.addrtuple = ("Enric♥ Zìní", "enrico@example.org")
        self.date = datetime.datetime(2017, 6, 5, 4, 3, 2)
        self.args = {
            "from_email": self.person,
            "to": self.addrstr,
            "cc": [self.person, self.addrstr, self.addrtuple],
            "subject": "♥ Debian ♥",
            "date": self.date,
            "body": "Debian ♥ Debian",
        }

    def test_python(self):
        msg = build_python_message(**self.args)
        headers, body = parse_headers(msg.as_bytes())
        self.maxDiff = None
        self.assertEqual(headers, {
            b'Cc': (b'=?utf-8?b?T25kxZllaiBOb3bDvQ==?= <ondrej@example.org>, test@example.org, '
                    b'=?utf-8?b?RW5yaWPimaUgWsOsbsOt?= <enrico@example.org>'),
            b'Content-Transfer-Encoding': b'base64',
            b'Content-Type': b'text/plain; charset="utf-8"',
            b'Date': b'Mon, 05 Jun 2017 04:03:02 -0000',
            b'From': b'=?utf-8?b?T25kxZllaiBOb3bDvQ==?= <ondrej@example.org>',
            b'MIME-Version': b'1.0',
            b'Subject': b'=?utf-8?b?4pmlIERlYmlhbiDimaU=?=',
            b'To': b'test@example.org',
        })
        self.assertEqual(body, [b'RGViaWFuIOKZpSBEZWJpYW4='])

    def test_django_backend(self):
        from django.core.mail import EmailMessage
        from django.core.mail.message import sanitize_address
        import email.utils
        import time
        import datetime
        msg = EmailMessage(
            from_email="♥ <heart@example.org>",
            to=[
                "Enrico <enrico@example.org>",
                _to_django_addr(("Enrico, Enrico", "enrico@example.org")),
                _to_django_addr(("Æņŕíçö", "enrico@example.org")),
            ],
            subject="♥ ♥ ♥",
            body="Màil bødy with ünicode charactærs",
            headers={
                'Date': email.utils.formatdate(time.mktime(datetime.datetime(2020, 8, 1, 0).timetuple())),
                'Message-ID': '<159783320478.499050.5798218801215253964@ploma.enricozini.org>',
            },
        )
        res = msg.message().as_bytes().splitlines()
        self.assertEqual(res, [
            b'Content-Type: text/plain; charset="utf-8"',
            b'MIME-Version: 1.0',
            b'Content-Transfer-Encoding: 8bit',
            b'Subject: =?utf-8?b?4pmlIOKZpSDimaU=?=',
            b'From: =?utf-8?b?4pml?= <heart@example.org>',
            b'To: Enrico <enrico@example.org>, "Enrico, Enrico" <enrico@example.org>,',
            b' =?utf-8?b?w4bFhsWVw63Dp8O2?= <enrico@example.org>',
            b'Date: Sat, 01 Aug 2020 00:00:00 -0000',
            b'Message-ID: <159783320478.499050.5798218801215253964@ploma.enricozini.org>',
            b'',
            b'M\xc3\xa0il b\xc3\xb8dy with \xc3\xbcnicode charact\xc3\xa6rs'
        ])

    def test_django(self):
        from process.email import build_django_message
        msg = build_django_message(**self.args)
        headers, body = parse_headers(msg.message().as_bytes())
        self.maxDiff = None

        self.assertIsNotNone(headers.pop(b"Message-ID"))
        cte = headers.pop(b'Content-Transfer-Encoding')
        self.assertIn(cte, [b"base64", b"8bit"])

        self.assertEqual(headers, {
            b'Cc': (
                b'=?utf-8?b?T25kxZllaiBOb3bDvQ==?= <ondrej@example.org>, test@example.org, '
                b'=?utf-8?b?RW5yaWPimaUgWsOsbsOt?= <enrico@example.org>'),
            b'Content-Type': b'text/plain; charset="utf-8"',
            b'Date': b'Mon, 05 Jun 2017 04:03:02 -0000',
            b'From': b'=?utf-8?b?T25kxZllaiBOb3bDvQ==?= <ondrej@example.org>',
            b'MIME-Version': b'1.0',
            b'Subject': b'=?utf-8?b?4pmlIERlYmlhbiDimaU=?=',
            b'To': b'test@example.org',
        })

        # Django 1.11 no longer base64-encodes everything (#27333 upstream)
        if cte == b'base64':
            self.assertEqual(body, [b'RGViaWFuIOKZpSBEZWJpYW4='])
        else:
            self.assertEqual(body, ['Debian ♥ Debian'.encode("utf8")])

    def test_email_invalid(self):
        my_email = "Enrico, Enrico <enrico@example.org>"
        my_email2 = "Pierre-Elliott \n Bécue <peb@example.org>"

        with self.assertRaises(ValueError):
            s = _to_django_addr(my_email)

        with self.assertRaises(ValueError):
            s = _to_django_addr(my_email2)

    def test_email_with_weird_parts(self):
        xn_domain = "Enrico <enrico@éxample.org>"
        encoded_local = "Pierre-Elliott Bécue <bécue@example.org>"
        maxi_best_of = "Pierre-Elliott Bécue <bécue@bécue.org>"
        email_with_commas = '"Pierre-Elliott, Charles, Maxime, Antoine, Bécue" <peb@example.org>'
        maxi_best_of_tuple = ('Pierre-Elliott "PEB" Bécue, best of all known devs', 'pebécue@bécue.org')

        s = _to_django_addr(xn_domain)
        self.assertEqual(s, "Enrico <enrico@xn--xample-9ua.org>")

        s = _to_django_addr(encoded_local)
        self.assertEqual(s, "=?utf-8?q?Pierre-Elliott_B=C3=A9cue?= <=?utf-8?b?YsOpY3Vl?=@example.org>")

        s = _to_django_addr(maxi_best_of)
        self.assertEqual(s, "=?utf-8?q?Pierre-Elliott_B=C3=A9cue?= <=?utf-8?b?YsOpY3Vl?=@xn--bcue-bpa.org>")

        s = _to_django_addr(email_with_commas)
        self.assertEqual(s, "=?utf-8?q?Pierre-Elliott=2C_Charles=2C_Maxime=2C_Antoine=2C_B=C3=A9cue?= <peb@example.org>")

        s = _to_django_addr(self.person)
        self.assertEqual(s, "=?utf-8?b?T25kxZllaiBOb3bDvQ==?= <ondrej@example.org>")

        s = _to_django_addr(maxi_best_of_tuple)
        self.assertEqual(s, "=?utf-8?q?Pierre-Elliott_=22PEB=22_B=C3=A9cue=2C_best_of_all_known_devs?= <=?utf-8?q?peb=C3=A9cue?=@xn--bcue-bpa.org>")
