from __future__ import annotations
from unittest import mock
import datetime
from django.utils.timezone import now
from django.test import TestCase
from backend.unittest import HousekeepingMixin
from process.unittest import ProcessFixtureMixin, test_fingerprint1, test_fingerprint2, test_fingerprint3
from process.housekeeping import OpenApprovedRTTickets
from backend import const


class TestFDApproval(HousekeepingMixin, ProcessFixtureMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.create_person("app", status=const.STATUS_DC)
        cls.processes.create("app", person=cls.persons.app, applying_for=const.STATUS_DD_U)
        cls.fingerprints.create(
                "dd_u", person=cls.persons.dd_u, fpr=test_fingerprint1, is_active=True, audit_skip=True)
        cls.fingerprints.create(
                "fd", person=cls.persons.fd, fpr=test_fingerprint2, is_active=True, audit_skip=True)
        cls.fingerprints.create(
                "dam", person=cls.persons.dam, fpr=test_fingerprint3, is_active=True, audit_skip=True)

    def _approve_process(self, approved_by, timestamp=None):
        """
        Add an approval statement to the process by the given person
        """
        if timestamp is None:
            timestamp = now()

        approval = self.processes.app.requirements.get(type="approval")

        # File an approval statement
        approval.statements.create(
                statement="approved",
                fpr=self.fingerprints[approved_by],
                uploaded_by=self.persons[approved_by],
                uploaded_time=timestamp)

        # Mark the requirement as approved, if it wasn't already
        if approval.approved_by is None:
            approval.approved_by = self.persons[approved_by]
            approval.approved_time = timestamp
            approval.save()

        # Mark the process as approved, if it wasn't already
        if self.processes.app.approved_by is None:
            self.processes.app.approved_by = self.persons[approved_by]
            self.processes.app.approved_time = timestamp
            self.processes.app.save()

    def assertNoRT(self):
        self.processes.app.refresh_from_db()
        self.assertIsNone(self.processes.app.rt_ticket)

    def assertRTEqual(self, number):
        self.processes.app.refresh_from_db()
        self.assertEqual(self.processes.app.rt_ticket, number)

    def test_no_approvals(self):
        with self.mock_rt_response(1):
            # Process has no approvals
            self.assertNoRT()

            # It doesn't get sent to RT right now
            self.run_housekeeping_task(OpenApprovedRTTickets)
            self.assertNoRT()

            # It doesn't get sent to RT later
            mock_ts = now() + datetime.timedelta(days=7)
            with mock.patch("process.maintenance.now", return_value=mock_ts):
                self.run_housekeeping_task(OpenApprovedRTTickets)
            self.assertNoRT()

    def test_approvals_by_nonstaff(self):
        # Process has an approval by someone who is not a staff member.
        # This should never happen, but if it happens, we make sure it doesn't
        # get worse
        with self.mock_rt_response(1):
            self._approve_process("dd_u")
            self.assertNoRT()

            # It doesn't get sent to RT right now
            self.run_housekeeping_task(OpenApprovedRTTickets)
            self.assertNoRT()

            # It doesn't get sent to RT later
            mock_ts = now() + datetime.timedelta(days=7)
            with mock.patch("process.maintenance.now", return_value=mock_ts):
                self.run_housekeeping_task(OpenApprovedRTTickets)
            self.assertNoRT()

    def test_approvals_by_fd_unchallenged(self):
        # Approval by FD, with no later comments, gets sent to RT only a week later
        with self.mock_rt_response(1):
            self._approve_process("fd")
            self.assertNoRT()

            # It doesn't get sent to RT right now
            self.run_housekeeping_task(OpenApprovedRTTickets)
            self.assertNoRT()

            # It does get sent to RT a week later
            mock_ts = now() + datetime.timedelta(days=7)
            with mock.patch("process.maintenance.now", return_value=mock_ts):
                self.run_housekeeping_task(OpenApprovedRTTickets)
            self.assertRTEqual(1)

    def test_approvals_by_fd_challenged_in_process(self):
        # Approval by FD, with later comments, does not get sent to RT
        with self.mock_rt_response(1):
            self._approve_process("fd")
            self.assertNoRT()

            # It doesn't get sent to RT right now
            self.run_housekeeping_task(OpenApprovedRTTickets)
            self.assertNoRT()

            # Someone logs a complaint on the process
            self.processes.app.add_log(
                    self.persons.dd_nu, "Hey! Wait! I've got a new complaint",
                    logdate=now() + datetime.timedelta(days=1))

            # It still doesn't get sent to RT right now
            self.run_housekeeping_task(OpenApprovedRTTickets)
            self.assertNoRT()

            # It also does not get sent to RT two weeks later
            mock_ts = now() + datetime.timedelta(days=14)
            with mock.patch("process.maintenance.now", return_value=mock_ts):
                self.run_housekeeping_task(OpenApprovedRTTickets)
            self.assertNoRT()

    def test_approvals_by_fd_challenged_in_requirement(self):
        # Approval by FD, with later comments, does not get sent to RT
        with self.mock_rt_response(1):
            self._approve_process("fd")
            self.assertNoRT()

            # It doesn't get sent to RT right now
            self.run_housekeeping_task(OpenApprovedRTTickets)
            self.assertNoRT()

            # Someone logs a complaint on the process
            self.processes.app.requirements.get(type="approval").add_log(
                    self.persons.dd_nu, "Hey! Wait! I've got a new complaint",
                    logdate=now() + datetime.timedelta(days=1))

            # It still doesn't get sent to RT right now
            self.run_housekeeping_task(OpenApprovedRTTickets)
            self.assertNoRT()

            # It also does not get sent to RT two weeks later
            mock_ts = now() + datetime.timedelta(days=14)
            with mock.patch("process.maintenance.now", return_value=mock_ts):
                self.run_housekeeping_task(OpenApprovedRTTickets)
            self.assertNoRT()

    def test_approvals_by_fd_challenged_answered(self):
        # Approval by FD, with later comments, and a further approval by FD, does get sent to RT
        with self.mock_rt_response(1):
            self._approve_process("fd")

            self.processes.app.add_log(
                    self.persons.dd_nu, "Hey! Wait! I've got a new complaint",
                    logdate=now() + datetime.timedelta(days=1))

            self._approve_process("fd", timestamp=now() + datetime.timedelta(days=2))

            # It doesn't get sent to RT right now
            self.run_housekeeping_task(OpenApprovedRTTickets)
            self.assertNoRT()

            # It does not get sent to RT a week after the first FD approval
            mock_ts = now() + datetime.timedelta(days=7)
            with mock.patch("process.maintenance.now", return_value=mock_ts):
                self.run_housekeeping_task(OpenApprovedRTTickets)
            self.assertNoRT()

            # It does get sent to RT a week after the last FD approval
            mock_ts = now() + datetime.timedelta(days=9)
            with mock.patch("process.maintenance.now", return_value=mock_ts):
                self.run_housekeeping_task(OpenApprovedRTTickets)
            self.assertRTEqual(1)
